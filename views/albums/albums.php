<!DOCTYPE html>
<html lang="zh-tw">
<?php include "../includes/in-head.php" ?>

<body>

    <!-- scrollToTop -->
    <!-- ================ -->
    <div class="scrollToTop circle"><i class="fa fa-angle-up"></i></div>

    <!-- page wrapper start -->
    <!-- ================ -->
    <div class="page-wrapper">

        <?php include "../includes/header.php" ?>

        <div class="banner in-banner dark-translucent-bg" style="background-image: url(../../assets/images/home/bg-idx-about.jpg);">
            <div class="container">
                <div class="row pv-4r">
                    <div class="col-lg-7">
                        <div class="banner-context text-left">
                            <h2 class="page-title title text-default mb-3" data-animation-effect="fadeIn"
                                data-effect-delay="100">工程實績</h2>
                            <p data-animation-effect="fadeIn" data-effect-delay="100">傳承40多年車體製造經驗<br>精攻於升降機尾門開發與設計</p>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="dark">
                            <ol class="breadcrumb d-flex justify-content-lg-end">
                                <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="index.html">首頁</a></li>
                                <li class="breadcrumb-item active">工程實績</li>
                            </ol>
                        </div>
                        <!-- breadcrumb end -->
                    </div>
                </div>
            </div>

        </div>
        <!-- banner end -->

        <div id="page-start"></div>

        <section class="in-wrap">
            <div class="album-list-wrap">
                <div class="d-flex justify-content-center mt-5 mb-4 container-fluid">
                    <div class="filters filters-cc">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a class="nav-link active" href="#" data-filter="*">All</a></li>
                            <li class="nav-item"><a class="nav-link" href="#" data-filter=".albums-cate01">Category01</a></li>
                            <li class="nav-item"><a class="nav-link" href="#" data-filter=".albums-cate02">Category02</a></li>
                            <li class="nav-item"><a class="nav-link" href="#" data-filter=".albums-cate03">Category03</a></li>
                        </ul>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="isotope-container row grid-space-0 products-list">
                        <div class="col-md-6 col-lg-3 isotope-item albums-cate01">
                            <div class="product-item text-center">
                                <div class="product-content">
                                    <div class="product-image">
                                        <img src="../../upload/products/product01.jpg" alt="正大尾門油壓升降機有限公司">
                                    </div>
                                    <div class="detail-link d-flex flex-column justify-content-center align-items-center">
                                        <span class="px-4 text-center">實績相簿名稱</span>
                                        <a title="正大尾門" href="albums-detail.php" class="btn btn-default radius-50 letter-space-02">
                                            <span>詳 情</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 isotope-item albums-cate01">
                            <div class="product-item text-center">
                                <div class="product-content">
                                    <div class="product-image">
                                        <img src="../../upload/products/product02.jpg" alt="正大尾門油壓升降機有限公司">
                                    </div>
                                    <div class="detail-link d-flex flex-column justify-content-center align-items-center">
                                        <span class="px-4 text-center">實績相簿名稱</span>
                                        <a title="正大尾門" href="albums-detail.php" class="btn btn-default radius-50 letter-space-02">
                                            <span>詳 情</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 isotope-item albums-cate02">
                            <div class="product-item text-center">
                                <div class="product-content">
                                    <div class="product-image">
                                        <img src="../../upload/products/product03.jpg" alt="正大尾門油壓升降機有限公司">
                                    </div>
                                    <div class="detail-link d-flex flex-column justify-content-center align-items-center">
                                        <span class="px-4 text-center">實績相簿名稱</span>
                                        <a title="正大尾門" href="albums-detail.php" class="btn btn-default radius-50 letter-space-02">
                                            <span>詳 情</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 isotope-item albums-cate02">
                            <div class="product-item text-center">
                                <div class="product-content">
                                    <div class="product-image">
                                        <img src="../../upload/products/product04.jpg" alt="正大尾門油壓升降機有限公司">
                                    </div>
                                    <div class="detail-link d-flex flex-column justify-content-center align-items-center">
                                        <span class="px-4 text-center">實績相簿名稱</span>
                                        <a title="正大尾門" href="albums-detail.php" class="btn btn-default radius-50 letter-space-02">
                                            <span>詳 情</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 isotope-item albums-cate02">
                            <div class="product-item text-center">
                                <div class="product-content">
                                    <div class="product-image">
                                        <img src="../../upload/products/product01.jpg" alt="正大尾門油壓升降機有限公司">
                                    </div>
                                    <div class="detail-link d-flex flex-column justify-content-center align-items-center">
                                        <span class="px-4 text-center">實績相簿名稱</span>
                                        <a title="正大尾門" href="albums-detail.php" class="btn btn-default radius-50 letter-space-02">
                                            <span>詳 情</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 isotope-item albums-cate03">
                            <div class="product-item text-center">
                                <div class="product-content">
                                    <div class="product-image">
                                        <img src="../../upload/products/product05.jpg" alt="正大尾門油壓升降機有限公司">
                                    </div>
                                    <div class="detail-link d-flex flex-column justify-content-center align-items-center">
                                        <span class="px-4 text-center">實績相簿名稱</span>
                                        <a title="正大尾門" href="albums-detail.php" class="btn btn-default radius-50 letter-space-02">
                                            <span>詳 情</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 isotope-item albums-cate03">
                            <div class="product-item text-center">
                                <div class="product-content">
                                    <div class="product-image">
                                        <img src="../../upload/products/product01.jpg" alt="正大尾門油壓升降機有限公司">
                                    </div>
                                    <div class="detail-link d-flex flex-column justify-content-center align-items-center">
                                        <span class="px-4 text-center">實績相簿名稱</span>
                                        <a title="正大尾門" href="albums-detail.php" class="btn btn-default radius-50 letter-space-02">
                                            <span>詳 情</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php include "../includes/footer.php" ?>
    </div>
    <!-- page-wrapper end -->

    <!--Plugins-->
    <!-- Jquery and Bootstap core js files -->
    <script src="../../assets/plugins/jquery.min.js"></script>
    <script src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Appear javascript -->
    <script src="../../assets/plugins/waypoints/jquery.waypoints.min.js"></script>
    <script src="../../assets/plugins/waypoints/sticky.min.js"></script>
    <!-- Slick carousel javascript -->
    <script src="../../assets/plugins/slick/slick.min.js"></script>
    <!-- Isotope javascript -->
    <script src="../../assets/plugins/isotope/imagesloaded.pkgd.min.js"></script>
    <script src="../../assets/plugins/isotope/isotope.pkgd.min.js"></script>
    <!-- Initialization of Plugins -->
    <script src="../../assets/js/template.js"></script>
    <!-- Custom Scripts -->
    <script src="../../assets/js/clicker_box.js"></script>
    <script src="../../assets/js/custom.js"></script>

</body>

</html>