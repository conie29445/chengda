<!DOCTYPE html>
<html lang="zh-tw">
<?php include "../includes/in-head.php" ?>

<body>

    <!-- scrollToTop -->
    <!-- ================ -->
    <div class="scrollToTop circle"><i class="fa fa-angle-up"></i></div>

    <!-- page wrapper start -->
    <!-- ================ -->
    <div class="page-wrapper">

        <?php include "../includes/header.php" ?>

        <div class="banner in-banner dark-translucent-bg" style="background-image: url(../../assets/images/home/bg-idx-about.jpg);">
            <div class="container">
                <div class="row pv-4r">
                    <div class="col-lg-7">
                        <div class="banner-context text-left">
                            <h2 class="page-title title text-default mb-3" data-animation-effect="fadeIn"
                                data-effect-delay="100">關於正大</h2>
                            <p data-animation-effect="fadeIn" data-effect-delay="100">傳承40多年車體製造經驗<br>精攻於升降機尾門開發與設計</p>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="dark">
                            <ol class="breadcrumb d-flex justify-content-lg-end">
                                <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="index.html">首頁</a></li>
                                <li class="breadcrumb-item active">關於正大</li>
                            </ol>
                        </div>
                        <!-- breadcrumb end -->
                    </div>
                </div>
            </div>

        </div>
        <!-- banner end -->

        <div id="page-start"></div>

        <section class="in-wrap">
            <div class="editor-wrap">
                <div class="about-wrap">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 bg-gray about-border">
                                <div class="about-context pv-6r px-3 px-lg-5" data-animation-effect="fadeInUp"
                                    data-effect-delay="80">
                                    <p class="title about-title mb-4">設立於1992年</p>
                                    <p>正大尾門前身為正大工業社</p>
                                    <p>創辦人以其身二十多年專業技術投入砂石車體、傾卸車斗、曳引車等各式車體製造、維修，並於2004年轉型為尾門製造。</p>
                                </div>
                            </div>

                            <div class="col-md-6 about-pic-wrap d-flex align-items-center" style="background-image: url(../../upload/about/about01.jpg)">
                                <!-- <div class="about-pic about-pic01">
                                    <img src="../upload/about/about01.jpg" alt="" data-animation-effect="fadeInUp" data-effect-delay="200">
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="about-wrap px-3 about-bg" style="background-image: url(../../upload/about/about02.jpg)">
                    <div class="text-center about-context pv-7r" data-animation-effect="fadeInUp" data-effect-delay="400">
                        <h3 class="about-title about-slogan mb-4">「以客為尊，品質至上」</h3>
                        <p>二十多年的深耕與努力，用著「以客為尊，品質至上」的理念<br>不斷的創新與優化，製造出貼近客戶需求、品質安全的產品，為您提供安全、穩定、省力的工具。</p>
                    </div>
                </div>

                <div class="about-wrap">
                    <div class="container-fluid">
                        <div class="row d-flex align-items-stretch">
                            <div class="col-lg-6 p-4 bg-fixed bg-black bg-about-service" style="background-image: url(../../upload/about/about03.jpg)"></div>
                            <div class="col-lg-6 bg-gray about-border">
                                <div class="service-items-list px-5">
                                    <div class="d-block d-md-flex service-item my-4 my-lg-5" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                        <span class="pr-3 text-center icon-service-wrap d-flex align-items-center justify-content-center mb-2 mb-md-0"><img src="../../assets/images/icons/icon-s01.svg" class="icon-service"></span>
                                        <div class="d-block d-sm-flex align-items-start">
                                            <div class="text-default text-center pr-3" style="min-width: 8em; font-weight: bold;">尾門製造</div>
                                            <div class="text-center text-sm-left service-description">專門製造各式、特殊之尾門升降機，單缸、雙缸、五缸等，材質包含黑鐵、白鐵以及鋁合金尾門製造。</div>
                                        </div>
                                    </div>
                                    <div class="d-block d-md-flex service-item my-4 my-lg-5" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                        <span class="pr-3 text-center icon-service-wrap d-flex align-items-center justify-content-center mb-2 mb-md-0"><img src="../../assets/images/icons/icon-s02.svg" class="icon-service"></span>
                                        <div class="d-block d-sm-flex align-items-start">
                                            <div class="text-default text-center pr-3" style="min-width: 8em; font-weight: bold;"><span class="small">國外各品牌尾門<br></span>銷售、安裝</div>
                                            <div class="text-center text-sm-left service-description">銷售國外大品牌之尾門升降機，並提供安裝、維修等服務。</div>
                                        </div>
                                    </div>
                                    <div class="d-block d-md-flex service-item my-4 my-lg-5" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                        <span class="pr-3 text-center icon-service-wrap d-flex align-items-center justify-content-center mb-2 mb-md-0"><img src="../../assets/images/icons/icon-s03.svg" class="icon-service"></span>
                                        <div class="d-block d-sm-flex align-items-start">
                                            <div class="text-default text-center pr-3" style="min-width: 8em; font-weight: bold;">尾門維修</div>
                                            <div class="text-center text-sm-left service-description">專門維修國內外各品牌之尾門升降機，同時兼持以客為尊的理念，協助客戶優先處理維修事宜。</div>
                                        </div>
                                    </div>
                                    <div class="d-block d-md-flex service-item my-4 my-lg-5" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                        <span class="pr-3 text-center icon-service-wrap d-flex align-items-center justify-content-center mb-2 mb-md-0"><img src="../../assets/images/icons/icon-s04.svg" class="icon-service"></span>
                                        <div class="d-block d-sm-flex align-items-start">
                                            <div class="text-default text-center pr-3" style="min-width: 8em; font-weight: bold;">銷售各式<br>尾門配件</div>
                                            <div class="text-center text-sm-left service-description">無線遙控器、數位遙控器、馬達、油壓缸、油管等週邊相關配件。</div>
                                        </div>
                                    </div>
                                    <div class="d-block d-md-flex service-item my-4 my-lg-5" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                        <span class="pr-3 text-center icon-service-wrap d-flex align-items-center justify-content-center mb-2 mb-md-0"><img src="../../assets/images/icons/icon-s05.svg" class="icon-service"></span>
                                        <div class="d-block d-sm-flex align-items-start">
                                            <div class="text-default text-center pr-3" style="min-width: 8em; font-weight: bold;">鋁合金車體<br>製造</div>
                                            <div class="text-center text-sm-left service-description">專業鋁合金車體製造，提供輕量化車體、配件製造，以提高車子性能、降低車身重量，同時符合現行法令要求。</div>
                                        </div>
                                    </div>
                                    <div class="d-block d-md-flex service-item my-4 my-lg-5" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                        <span class="pr-3 text-center icon-service-wrap d-flex align-items-center justify-content-center mb-2 mb-md-0"><img src="../../assets/images/icons/icon-s06.svg" class="icon-service"></span>
                                        <div class="d-block d-sm-flex align-items-start">
                                            <div class="text-default text-center pr-3" style="min-width: 8em; font-weight: bold;">各式車體<br>維修</div>
                                            <div class="text-center text-sm-left service-description">維修項目包括車斗、車廂、鷗翼車廂及各式特殊特裝車。</div>
                                        </div>
                                    </div>
                                    <div class="d-block d-md-flex service-item my-4 my-lg-5" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                        <span class="pr-3 text-center icon-service-wrap d-flex align-items-center justify-content-center mb-2 mb-md-0"><img src="../../assets/images/icons/icon-s07.svg" class="icon-service"></span>
                                        <div class="d-block d-sm-flex align-items-start">
                                            <div class="text-default text-center pr-3" style="min-width: 8em; font-weight: bold;">代客<br>驗車、變更</div>
                                            <div class="text-center text-sm-left service-description">專門製造各式、特殊之尾門升降機，單缸、雙缸、五缸等，材質包含黑鐵、白鐵以及鋁合金尾門製造。</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <?php include "../includes/footer.php" ?>
    </div>
    <!-- page-wrapper end -->

    <!--Plugins-->
    <!-- Jquery and Bootstap core js files -->
    <script src="../../assets/plugins/jquery.min.js"></script>
    <script src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Appear javascript -->
    <script src="../../assets/plugins/waypoints/jquery.waypoints.min.js"></script>
    <script src="../../assets/plugins/waypoints/sticky.min.js"></script>
    <!-- Slick carousel javascript -->
    <script src="../../assets/plugins/slick/slick.min.js"></script>
    <!-- Initialization of Plugins -->
    <script src="../../assets/js/template.js"></script>
    <!-- Custom Scripts -->
    <script src="../../assets/js/clicker_box.js"></script>
    <script src="../../assets/js/custom.js"></script>

</body>

</html>