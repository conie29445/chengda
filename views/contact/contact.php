<!DOCTYPE html>
<html lang="zh-tw">
<?php include "../includes/in-head.php" ?>

<body>

    <!-- scrollToTop -->
    <!-- ================ -->
    <div class="scrollToTop circle"><i class="fa fa-angle-up"></i></div>

    <!-- page wrapper start -->
    <!-- ================ -->
    <div class="page-wrapper">

        <?php include "../includes/header.php" ?>

        <div class="banner in-banner dark-translucent-bg" style="background-image: url(../../assets/images/home/bg-idx-about.jpg);">
            <div class="container">
                <div class="row pv-4r">
                    <div class="col-lg-7">
                        <div class="banner-context text-left">
                            <h2 class="page-title title text-default mb-3" data-animation-effect="fadeIn"
                                data-effect-delay="100">聯絡我們</h2>
                            <p data-animation-effect="fadeIn" data-effect-delay="100">傳承40多年車體製造經驗<br>精攻於升降機尾門開發與設計</p>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="dark">
                            <ol class="breadcrumb d-flex justify-content-lg-end">
                                <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="index.html">首頁</a></li>
                                <li class="breadcrumb-item active">聯絡我們</li>
                            </ol>
                        </div>
                        <!-- breadcrumb end -->
                    </div>
                </div>
            </div>

        </div>
        <!-- banner end -->

        <div id="page-start"></div>

        <section class="in-wrap">
            <div class="contact-wrap">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 bg-gray page-border">
                            <div class="px-3 px-lg-5 mt-5 mb-2">
                                <h1 class="page-title mb-4">請填寫您的資料與訊息</h1>
                                <p>若有任何問題，可利用以下表單與我們聯絡，我們將儘快回覆您，謝謝。</p>
                            </div>

                            <form class="margin-clear py-3 px-3 px-lg-5">
                                <div class="form-group has-feedback">
                                    <label for="name">您的姓名*</label>
                                    <input type="text" class="form-control" id="name" placeholder="Name" required>
                                    <i class="fa fa-user form-control-feedback"></i>
                                </div>
                                <div class="form-group has-feedback">
                                    <label for="email">Email*</label>
                                    <input type="email" class="form-control" id="email" aria-describedby="emailHelperText"
                                        placeholder="Enter email" required>
                                    <i class="fa fa-envelope form-control-feedback"></i>
                                </div>
                                <div class="form-group has-feedback">
                                    <label for="subject">主要目的*</label>
                                    <input type="text" class="form-control" id="subject" placeholder="Subject" required>
                                    <i class="fa fa-navicon form-control-feedback"></i>
                                </div>
                                <div class="form-group has-feedback">
                                    <label for="message">詢問訊息*</label>
                                    <textarea class="form-control" rows="6" id="message" placeholder="Message" required></textarea>
                                    <i class="fa fa-pencil form-control-feedback"></i>
                                </div>
                                <small class="form-text mt-2">機器人驗證，點圖可更新數字</small>
                                <div class="form-inline mb-3">
                                    <div class="form-group d-flex align-items-center">
                                        <label class="mb-1 pr-2">驗證*</label>
                                        <input type="text" class="mb-1 form-control form-control-small mr-2" placeholder="輸入驗證數字" required>
                                        <span class="mb-1 mr-2 pointer"><img src="../../assets/images/captcha-fake.jpg"
                                                alt=""></span>
                                    </div>
                                </div>
                                <input type="submit" value="送出表單" class="submit-button btn radius-50 btn-cta btn-lg letter-space-04">
                            </form>
                        </div>

                        <div class="col-md-6">
                            <div class="p-3 p-lg-5">
                                <div class="contact-info">
                                    <h4>聯絡資訊</h4>
                                    <h3 class="mt-5 text-default">正大尾門油壓升降機有限公司</h3>
                                    <p>(各式特殊升降機、五軸升降機、鋁合金升降機、鋁合金車體製造)</p>
                                    <ul class="contact-info-list list">
                                        <li><i class="fa fa-home pr-10"></i><a href="https://goo.gl/maps/DGqjQMCDQG72" target="_blank">彰化縣伸港鄉什股路1-51號</a></li>
                                        <li><i class="fa fa-phone pr-10"></i> <a href="tel:04-7988856">04-7988856</a></li>
                                        <li><i class="fa fa-print pr-10 pl-1"></i> 04-7986409</li>
                                        <li><i class="fa fa-envelope pr-10"></i><a href="mailto:chengda.taillift@gmail.com">chengda.taillift@gmail.com</a></li>
                                    </ul>
                                </div>
                                <!-- 記得載入 Google Maps javascript-->
                                <div class="map-wrap border">
                                    <div id="map-canvas"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php include "../includes/footer.php" ?>
    </div>
    <!-- page-wrapper end -->

    <!--Plugins-->
    <!-- Jquery and Bootstap core js files -->
    <script src="../../assets/plugins/jquery.min.js"></script>
    <script src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Appear javascript -->
    <script src="../../assets/plugins/waypoints/jquery.waypoints.min.js"></script>
    <script src="../../assets/plugins/waypoints/sticky.min.js"></script>
    <!-- Slick carousel javascript -->
    <script src="../../assets/plugins/slick/slick.min.js"></script>
    <!-- Initialization of Plugins -->
    <script src="../../assets/js/template.js"></script>
    <!-- Google Maps javascript -->
    <script src="https://maps.googleapis.com/maps/api/js?key=your_google_map_key"></script>
    <script src="../../assets/js/google.map.config.js"></script>
    <!-- Custom Scripts -->
    <script src="../../assets/js/clicker_box.js"></script>
    <script src="../../assets/js/custom.js"></script>

</body>

</html>