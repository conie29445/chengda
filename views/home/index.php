<!DOCTYPE html>
<html lang="zh-tw">
<?php include "../includes/home-head.php" ?>

<body class="gradient-background-header transparent-header front-page ">

    <!-- scrollToTop -->
    <!-- ================ -->
    <div class="scrollToTop circle"><i class="fa fa-angle-up"></i></div>

    <!-- page wrapper start -->
    <!-- ================ -->
    <div class="page-wrapper">

        <?php include "../includes/header.php" ?>

        <div class="banner clearfix home-banner">

            <!-- slideshow start -->
            <!-- ================ -->
            <div class="slideshow">

                <!-- slider revolution start -->
                <!-- ================ -->
                <div class="slider-revolution-5-container">
                    <div id="slider-banner-fullscreen" class="slider-banner-fullscreen rev_slider" data-version="5.0">
                        <ul class="slides">
                            <!-- slide 1 start -->
                            <!-- ================ -->
                            <li data-transition="crossfade" data-slotamount="default" data-masterspeed="default"
                                data-title="以客為尊，品質至上">

                                <!-- main image -->
                                <img src="../../upload/home/slide1.jpg" alt="slidebg1" data-bgposition="center top"
                                    data-bgrepeat="no-repeat" data-bgfit="cover" class="rev-slidebg">

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption caption-box-cc text-center" data-x="center" data-y="center"
                                    data-start="600" data-whitespace="normal" data-transform_idle="o:1;"
                                    data-transform_in="y:[100%];sX:1;sY:1;o:0;s:1150;e:Power4.easeInOut;"
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">
                                    <h2 class="title bn-title large_white text-shadows">正大尾門</h2>
                                    <h4 class="mb-0 text-white text-shadows">以客為尊，品質至上</h4>
                                </div>
                                <div class="tp-caption" data-x="center" data-y="bottom" data-voffset="100" data-start="1250"
                                    data-transform_idle="o:1;" data-transform_in="y:[100%];sX:1;sY:1;o:0;s:2000;e:Power4.easeInOut;"
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">
                                    <a href="#page-start" class="btn btn-lg moving smooth-scroll"><i class="fa fa-angle-down"></i><i
                                            class="fa fa-angle-down"></i><i class="fa fa-angle-down"></i></a>
                                </div>

                            </li>
                            <!-- slide 1 end -->

                            <!-- slide 2 start -->
                            <!-- ================ -->
                            <li data-transition="crossfade" data-slotamount="default" data-masterspeed="default"
                                data-title="以客為尊，品質至上">

                                <!-- main image -->
                                <img src="../../upload/home/slide2.jpg" alt="slidebg2" data-bgposition="center top"
                                    data-bgrepeat="no-repeat" data-bgfit="cover" class="rev-slidebg">

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption caption-box-cc text-center" data-x="center" data-y="center"
                                    data-start="600" data-whitespace="normal" data-transform_idle="o:1;"
                                    data-transform_in="y:[100%];sX:1;sY:1;o:0;s:1150;e:Power4.easeInOut;"
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">
                                    <h2 class="title bn-title large_white text-shadows">正大尾門</h2>
                                    <h4 class="mb-0 text-white text-shadows">以客為尊，品質至上</h4>
                                </div>
                                <div class="tp-caption" data-x="center" data-y="bottom" data-voffset="100" data-start="1250"
                                    data-transform_idle="o:1;" data-transform_in="y:[100%];sX:1;sY:1;o:0;s:2000;e:Power4.easeInOut;"
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">
                                    <a href="#page-start" class="btn btn-lg moving smooth-scroll"><i class="fa fa-angle-down"></i><i
                                            class="fa fa-angle-down"></i><i class="fa fa-angle-down"></i></a>
                                </div>

                            </li>
                            <!-- slide 2 end -->
                        </ul>
                    </div>
                </div>
                <!-- slider revolution end -->

            </div>
            <!-- slideshow end -->

        </div>
        <!-- banner end -->

        <div id="page-start"></div>

        <!-- services section -->
        <!-- ================ -->
        <section class="pv-30 idx-service-wrap" style="background-image: url(../../assets/images/bg-1280.jpg)">
            <div class="container-fluid">
                <div class="row justify-content-lg-center">
                    <div class="col-lg-8">
                        <h2 class="text-center letter-space-03 py-5 text-default">服務項目</h2>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-6 col-md-4 col-lg-3 idx-service-item">
                        <div class="hc-item-box text-center hc-element-invisible" data-animation-effect="fadeInDownSmall"
                            data-effect-delay="100">
                            <span class="icon large"><img src="../../assets/images/icons/icon-s01.svg" alt=""></span>
                            <h4 class="letter-space-02">尾門製造</h4>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3 idx-service-item">
                        <div class="hc-item-box text-center hc-element-invisible" data-animation-effect="fadeInDownSmall"
                            data-effect-delay="100">
                            <span class="icon large"><img src="../../assets/images/icons/icon-s02.svg" alt=""></span>
                            <h4 class="letter-space-01"><span class="small">國外各品牌尾門<br></span>銷售、安裝</h4>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3 idx-service-item">
                        <div class="hc-item-box text-center hc-element-invisible" data-animation-effect="fadeInDownSmall"
                            data-effect-delay="100">
                            <span class="icon large"><img src="../../assets/images/icons/icon-s03.svg" alt=""></span>
                            <h4 class="letter-space-02">尾門維修</h4>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3 idx-service-item">
                        <div class="hc-item-box text-center hc-element-invisible" data-animation-effect="fadeInDownSmall"
                            data-effect-delay="100">
                            <span class="icon large"><img src="../../assets/images/icons/icon-s04.svg" alt=""></span>
                            <h4 class="letter-space-02">銷售各式<br>尾門配件</h4>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3 idx-service-item">
                        <div class="hc-item-box text-center hc-element-invisible" data-animation-effect="fadeInDownSmall"
                            data-effect-delay="100">
                            <span class="icon large"><img src="../../assets/images/icons/icon-s05.svg" alt=""></span>
                            <h4 class="letter-space-02">鋁合金車體<br>製造</h4>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3 idx-service-item">
                        <div class="hc-item-box text-center hc-element-invisible" data-animation-effect="fadeInDownSmall"
                            data-effect-delay="100">
                            <span class="icon large"><img src="../../assets/images/icons/icon-s06.svg" alt=""></span>
                            <h4 class="letter-space-02">各式車體<br>維修</h4>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3 idx-service-item">
                        <div class="hc-item-box text-center hc-element-invisible" data-animation-effect="fadeInDownSmall"
                            data-effect-delay="100">
                            <span class="icon large"><img src="../../assets/images/icons/icon-s07.svg" alt=""></span>
                            <h4 class="letter-space-02">代客<br>驗車、變更</h4>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center btn-idx-service">
                    <a href="../about/about.php" class="btn radius-50 btn-cta btn-lg">服務介紹 <i class="fa fa-chevron-right"></i></a>
                </div>
            </div>
        </section>
        <!-- section end -->

        <!-- about start -->
        <!-- ================ -->
        <section class="idx-about main-container" style="background-image: url(../../assets/images/home/bg-idx-about.jpg)">
            <div class="container py-5">
                <div class="row">

                    <!-- main start -->
                    <!-- ================ -->
                    <div class="main col-12 py-5 px-3 px-lg-5 idx-about-content" style="background: #fff url(../../assets/images/home/bg-idx-about02.jpg) right bottom no-repeat">

                        <div class="row">
                            <div class="col-md-8 col-lg-5">
                                <h1 class="page-title text-default mb-4">關於正大</h1>
                                <div class="idx-about-context mb-4">
                                    <h4 class="mb-2"><b>設立於1992年</b></h4>
                                    <p class="mb-2">正大尾門前身為正大工業社，創辦人以其身二十多年專業技術投入砂石車體、傾卸車斗、曳引車等各式車體製造、維修，並於2004年轉型為尾門製造。
                                        <p class="mb-2">秉持「以客為尊，品質至上」的理念，不斷創新與優化，製造出貼近客戶需求、品質安全的產品...</p>
                                </div>
                                <h4 class="text-colored letter-space-02 mb-3">我們的優勢</h4>
                                <div class="idx-about-context">
                                    <ul class="pl-4">
                                        <li>全台唯一鋁合金尾門製造</li>
                                        <li>各式品牌、各種款式尾門維修</li>
                                        <li>客製化、精製化鋁合金車體</li>
                                    </ul>
                                </div>
                                <a class="btn radius-50 btn-cta-h" href="../about/about.php">了解更多...</a>
                            </div>

                            <!-- sidebar start -->
                            <!-- ================ -->
                            <div class="offset-lg-1 col-lg-6">
                                <p class="pic">
                                    <img src="../../assets/images/home/idx-about01.png" alt="">
                                </p>
                            </div>
                            <!-- sidebar end -->
                        </div>
                    </div>
                    <!-- main end -->

                </div>
            </div>
        </section>
        <!-- about end -->

        <!--  products section start -->
        <!-- ================ -->
        <section id="idx-projects" class="light-gray-bg">
            <div class="container-fluid">
                <div class="row grid-space-0 products-list">
                    <div class="col-12 col-md-6 col-lg-3 product-item">
                        <div class="product-content">
                            <div class="product-image">
                                <img src="../../upload/products/product01.jpg" alt="正大尾門油壓升降機有限公司">
                            </div>
                            <div class="detail-link d-flex flex-column justify-content-center align-items-center">
                                <span class="px-4 text-center">實績名稱</span>
                                <a title="正大尾門" href="../product/detail.php" class="btn btn-default radius-50 letter-space-02">
                                    <span>詳情</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 product-item">
                        <div class="product-content">
                            <div class="product-image">
                                <img src="../../upload/products/product02.jpg" alt="正大尾門油壓升降機有限公司">
                            </div>
                            <div class="detail-link d-flex flex-column justify-content-center align-items-center">
                                <span class="px-4 text-center">實績名稱實績名稱實績名稱實績名稱實績名稱實績名稱實績名稱</span>
                                <a title="正大尾門" href="../product/detail.php" class="btn btn-default radius-50 letter-space-02">
                                    <span>詳情</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 product-item">
                        <div class="product-content">
                            <div class="product-image">
                                <img src="../../upload/products/product03.jpg" alt="正大尾門油壓升降機有限公司">
                            </div>
                            <div class="detail-link d-flex flex-column justify-content-center align-items-center">
                                <span class="px-4 text-center">實績名稱</span>
                                <a title="正大尾門" href="../product/detail.php" class="btn btn-default radius-50 letter-space-02">
                                    <span>詳情</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 product-item">
                        <div class="product-content">
                            <div class="product-image">
                                <img src="../../upload/products/product04.jpg" alt="正大尾門油壓升降機有限公司">
                            </div>
                            <div class="detail-link d-flex flex-column justify-content-center align-items-center">
                                <span class="px-4 text-center">實績名稱</span>
                                <a title="正大尾門" href="../product/detail.php" class="btn btn-default radius-50 letter-space-02">
                                    <span>詳情</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 product-item">
                        <div class="product-content">
                            <div class="product-image">
                                <img src="../../upload/products/product05.jpg" alt="正大尾門油壓升降機有限公司">
                            </div>
                            <div class="detail-link d-flex flex-column justify-content-center align-items-center">
                                <span class="px-4 text-center">實績名稱</span>
                                <a title="正大尾門" href="../product/detail.php" class="btn btn-default radius-50 letter-space-02">
                                    <span>詳情</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 product-item">
                        <div class="product-content">
                            <div class="product-image">
                                <img src="../../upload/products/product01.jpg" alt="正大尾門油壓升降機有限公司">
                            </div>
                            <div class="detail-link d-flex flex-column justify-content-center align-items-center">
                                <span class="px-4 text-center">實績名稱</span>
                                <a title="正大尾門" href="../product/detail.php" class="btn btn-default radius-50 letter-space-02">
                                    <span>詳情</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 product-item">
                        <div class="product-content">
                            <div class="product-image">
                                <img src="../../upload/products/product02.jpg" alt="正大尾門油壓升降機有限公司">
                            </div>
                            <div class="detail-link d-flex flex-column justify-content-center align-items-center">
                                <span class="px-4 text-center">實績名稱</span>
                                <a title="正大尾門" href="../product/detail.php" class="btn btn-default radius-50 letter-space-02">
                                    <span>詳情</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 product-item">
                        <div class="product-content">
                            <div class="product-image">
                                <img src="../../upload/products/product03.jpg" alt="正大尾門油壓升降機有限公司">
                            </div>
                            <div class="detail-link d-flex flex-column justify-content-center align-items-center">
                                <span class="px-4 text-center">實績名稱</span>
                                <a title="正大尾門" href="../product/detail.php" class="btn btn-default radius-50 letter-space-02">
                                    <span>詳情</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section end -->
        <?php include "../includes/footer.php" ?>
    </div>
    <!-- page-wrapper end -->

    <!--Plugins-->
    <!-- Jquery and Bootstap core js files -->
    <script src="../../assets/plugins/jquery.min.js"></script>
    <script src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- jQuery Revolution Slider  -->
    <script src="../../assets/plugins/rs-plugin-5/js/jquery.themepunch.tools.min.js"></script>
    <script src="../../assets/plugins/rs-plugin-5/js/jquery.themepunch.revolution.min.js"></script>
    <!-- Appear javascript -->
    <script src="../../assets/plugins/waypoints/jquery.waypoints.min.js"></script>
    <script src="../../assets/plugins/waypoints/sticky.min.js"></script>
    <!-- Slick carousel javascript -->
    <script src="../../assets/plugins/slick/slick.min.js"></script>
    <!-- Initialization of Plugins -->
    <script src="../../assets/js/template.js"></script>
    <!-- Custom Scripts -->
    <script src="../../assets/js/clicker_box.js"></script>
    <script src="../../assets/js/custom.js"></script>

</body>

</html>