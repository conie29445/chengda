<!DOCTYPE html>
<html lang="zh-tw">
<?php include "../includes/in-head.php" ?>

<body>

    <!-- scrollToTop -->
    <!-- ================ -->
    <div class="scrollToTop circle"><i class="fa fa-angle-up"></i></div>

    <!-- page wrapper start -->
    <!-- ================ -->
    <div class="page-wrapper">

        <?php include "../includes/header.php" ?>

        <div class="banner in-banner dark-translucent-bg" style="background-image: url(../../assets/images/home/bg-idx-about.jpg);">
            <div class="container">
                <div class="row pv-4r">
                    <div class="col-lg-7">
                        <div class="banner-context text-left">
                            <h2 class="page-title title text-default mb-3" data-animation-effect="fadeIn"
                                data-effect-delay="100">最新消息</h2>
                            <p data-animation-effect="fadeIn" data-effect-delay="100">評選年度10大特優遊客中心以貼心而便利的服務贏得旅客歡心</p>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="dark">
                            <ol class="breadcrumb d-flex justify-content-lg-end">
                                <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="index.html">首頁</a></li>
                                <li class="breadcrumb-item active">最新消息</li>
                            </ol>
                        </div>
                        <!-- breadcrumb end -->
                    </div>
                </div>
            </div>

        </div>
        <!-- banner end -->

        <div id="page-start"></div>

        <section class="in-wrap">
        <div class="container page-border">
                <div class="news-detail row">
                    <div class="col-lg-4 bg-news"></div>
                    <div class="col-12 col-lg-8 py-5 pt-3 px-3 px-lg-5">
                        <h1 class="mb-3">評選年度10大特優遊客中心以貼心而便利的服務贏得旅客歡心</h1>
                        <p class="small mb-10"><i class="fa fa-calendar-o pr-1"></i> 2019.01.22</p>
                        <hr>
                        <p class="pic my-3">
                            <img src="../../upload/no-pic.jpg" alt="">
                        </p>
                        <div class="editor-wrap py-3">
                            <p>今（26日）由交通部觀光局主辦107年度借問站MVP榮耀激勵會暨最佳站長頒獎典禮熱鬧展開，由林坤源主任秘書與天下雜誌鄭宜媛總經理擔任頒獎人，邀請得獎站長、各縣市政府及風景區管理處代表出席與會，針對最佳站長、最佳人氣及最佳輔導單位分別進行頒獎。</p>
                            <p>今（26日）由交通部觀光局主辦107年度借問站MVP榮耀激勵會暨最佳站長頒獎典禮熱鬧展開，由林坤源主任秘書與天下雜誌鄭宜媛總經理擔任頒獎人，邀請得獎站長、各縣市政府及風景區管理處代表出席與會，針對最佳站長、最佳人氣及最佳輔導單位分別進行頒獎。</p>
                            <p>今（26日）由交通部觀光局主辦107年度借問站MVP榮耀激勵會暨最佳站長頒獎典禮熱鬧展開，由林坤源主任秘書與天下雜誌鄭宜媛總經理擔任頒獎人，邀請得獎站長、各縣市政府及風景區管理處代表出席與會，針對最佳站長、最佳人氣及最佳輔導單位分別進行頒獎。</p>
                        </div>
                        <div class="col-12 text-center pt-3">
                            <a href="list.php" class="btn btn-default-h radius-50">回列表</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php include "../includes/footer.php" ?>
    </div>
    <!-- page-wrapper end -->

    <!--Plugins-->
    <!-- Jquery and Bootstap core js files -->
    <script src="../../assets/plugins/jquery.min.js"></script>
    <script src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Appear javascript -->
    <script src="../../assets/plugins/waypoints/jquery.waypoints.min.js"></script>
    <script src="../../assets/plugins/waypoints/sticky.min.js"></script>
    <!-- Slick carousel javascript -->
    <script src="../../assets/plugins/slick/slick.min.js"></script>
    <!-- Initialization of Plugins -->
    <script src="../../assets/js/template.js"></script>
    <!-- Custom Scripts -->
    <script src="../../assets/js/clicker_box.js"></script>
    <script src="../../assets/js/custom.js"></script>

</body>

</html>