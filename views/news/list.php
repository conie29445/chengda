<!DOCTYPE html>
<html lang="zh-tw">
<?php include "../includes/in-head.php" ?>

<body>

    <!-- scrollToTop -->
    <!-- ================ -->
    <div class="scrollToTop circle"><i class="fa fa-angle-up"></i></div>

    <!-- page wrapper start -->
    <!-- ================ -->
    <div class="page-wrapper">

        <?php include "../includes/header.php" ?>

        <div class="banner in-banner dark-translucent-bg" style="background-image: url(../../assets/images/home/bg-idx-about.jpg);">
            <div class="container">
                <div class="row pv-4r">
                    <div class="col-lg-7">
                        <div class="banner-context text-left">
                            <h2 class="page-title title text-default mb-3" data-animation-effect="fadeIn"
                                data-effect-delay="100">最新消息</h2>
                            <p data-animation-effect="fadeIn" data-effect-delay="100">傳承40多年車體製造經驗<br>精攻於升降機尾門開發與設計</p>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="dark">
                            <ol class="breadcrumb d-flex justify-content-lg-end">
                                <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="index.html">首頁</a></li>
                                <li class="breadcrumb-item active">最新消息</li>
                            </ol>
                        </div>
                        <!-- breadcrumb end -->
                    </div>
                </div>
            </div>

        </div>
        <!-- banner end -->

        <div id="page-start"></div>

        <section class="in-wrap">
            <div class="container page-border">
                <div class="news-list pt-5 row justify-content-center">
                    <div class="image-box style-3-b col-lg-12 p-3 bg-gray">
                        <div class="row">
                            <div class="col-md-6 col-lg-4 col-xl-3">
                                <div class="overlay-container">
                                    <img src="../../upload/no-pic.jpg" alt="">
                                    <div class="overlay-to-top">
                                        <a href="detail.php">詳細</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-8 col-xl-9">
                                <div class="body">
                                    <h3 class="title mt-3 mb-1"><a href="detail.php">最新消息標題</a></h3>
                                    <p class="small mb-4"><i class="fa fa-calendar-o pr-1"></i> 2019.01.22</p>
                                    <a href="detail.php" class="radius-50 mr-4">詳細內容 <i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="image-box style-3-b col-lg-12 p-3 bg-gray">
                        <div class="row">
                            <div class="col-md-6 col-lg-4 col-xl-3">
                                <div class="overlay-container">
                                    <img src="../../upload/no-pic.jpg" alt="">
                                    <div class="overlay-to-top">
                                        <a href="detail.php">詳細</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-8 col-xl-9">
                                <div class="body">
                                    <h3 class="title mt-3 mb-1"><a href="detail.php">最新消息標題</a></h3>
                                    <p class="small mb-4"><i class="fa fa-calendar-o pr-1"></i> Feb, 2017</p>
                                    <a href="detail.php" class="radius-50 mr-4">詳細內容 <i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-center">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                            </a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                                <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </section>

        <?php include "../includes/footer.php" ?>
    </div>
    <!-- page-wrapper end -->

    <!--Plugins-->
    <!-- Jquery and Bootstap core js files -->
    <script src="../../assets/plugins/jquery.min.js"></script>
    <script src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Appear javascript -->
    <script src="../../assets/plugins/waypoints/jquery.waypoints.min.js"></script>
    <script src="../../assets/plugins/waypoints/sticky.min.js"></script>
    <!-- Slick carousel javascript -->
    <script src="../../assets/plugins/slick/slick.min.js"></script>
    <!-- Initialization of Plugins -->
    <script src="../../assets/js/template.js"></script>
    <!-- Custom Scripts -->
    <script src="../../assets/js/clicker_box.js"></script>
    <script src="../../assets/js/custom.js"></script>

</body>

</html>