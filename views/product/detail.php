<!DOCTYPE html>
<html lang="zh-tw">
<?php include "../includes/in-head.php" ?>

<body>

    <!-- scrollToTop -->
    <!-- ================ -->
    <div class="scrollToTop circle"><i class="fa fa-angle-up"></i></div>

    <!-- page wrapper start -->
    <!-- ================ -->
    <div class="page-wrapper">

        <?php include "../includes/header.php" ?>

        <div class="banner in-banner dark-translucent-bg" style="background-image: url(../../assets/images/home/bg-idx-about.jpg);">
            <div class="container">
                <div class="row pv-4r">
                    <div class="col-lg-7">
                        <div class="banner-context text-left">
                            <h2 class="page-title title text-default mb-3" data-animation-effect="fadeIn"
                                data-effect-delay="100">產品介紹</h2>
                            <p data-animation-effect="fadeIn" data-effect-delay="100">鋁合金升降機</p>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="dark">
                            <ol class="breadcrumb d-flex justify-content-lg-end">
                                <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="index.html">首頁</a></li>
                                <li class="breadcrumb-item">產品介紹</li>
                                <li class="breadcrumb-item"><a class="link-dark" href="list.php">分類1</a></li>
                                <li class="breadcrumb-item active">鋁合金升降機</li>
                            </ol>
                        </div>
                        <!-- breadcrumb end -->
                    </div>
                </div>
            </div>

        </div>
        <!-- banner end -->

        <div id="page-start"></div>

        <section class="in-wrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="aside-wrap col-lg-3 col-xl-2 page-border bg-dark dark light-gray-bg px-0">
                        <aside class="px-3">
                            <div class="sidebar py-3 py-lg-5 px-2 mt-0 mt-lg-40">
                                <h3 class="title pb-2">產品總覽</h3>
                                <div class="separator-2"></div>
                                <nav class="side-menu product-menu">
                                    <ul class="nav flex-column">
                                        <li class="nav-item"><a class="nav-link active" href="list.php">分類1</a></li>
                                        <li class="nav-item"><a class="nav-link" href="list.php">分類2</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </aside>
                    </div>
                    <div class="col-lg-9 col-xl-10 p-0">
                        <div class="product-search-wrap bg-gray">
                            <div class="px-3 py-1 d-flex justify-content-between justify-content-lg-end align-items-center">
                                <div id="btn-aside-trigger" class="d-flex justify-content-center align-items-center d-lg-none px-2 px-sm-3">產品選單<span
                                        class="fa fa-bars pl-1"></span></div>

                                <?php include "../pages/product-search.php" ?>
                            </div>
                        </div>

                        <section id="product-detail-wrap" class="p-5">
                            <div class="row">
                                <div class="col-12">
                                    <h1 class="mb-4">鋁合金升降機</h1>
                                    <hr>
                                </div>
                                <div class="col-xl-6 bg-gray">
                                    <div class="product-pics py-3">
                                        <div class="hc-shadow bordered">
                                            <div class="overlay-container">
                                                <img src="../../upload/products/product_pic.jpg" alt="">
                                                <a href="../../upload/products/product_pic.jpg" class="overlay-link popup-img" title="First image title">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="space-bottom"></div>
                                        <div class="row grid-space-20">
                                            <div class="col-3 mb-2">
                                                <div class="overlay-container">
                                                    <img src="../../upload/products/product_pic.jpg" alt="">
                                                    <a href="../../upload/products/product_pic.jpg" class="overlay-link small popup-img"
                                                        title="Second image title">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <div class="overlay-container">
                                                    <img src="../../upload/products/product02.jpg" alt="">
                                                    <a href="../../upload/products/product02.jpg" class="overlay-link small popup-img"
                                                        title="Third image title">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <div class="overlay-container">
                                                    <img src="../../upload/products/product01.jpg" alt="">
                                                    <a href="../../upload/products/product01.jpg" class="overlay-link small popup-img"
                                                        title="Fourth image title">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <div class="overlay-container">
                                                    <img src="../../upload/products/product_pic.jpg" alt="">
                                                    <a href="../../upload/products/product_pic.jpg" class="overlay-link small popup-img"
                                                        title="Fifth image title">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <div class="overlay-container">
                                                    <img src="../../upload/products/product01.jpg" alt="">
                                                    <a href="../../upload/products/product01.jpg" class="overlay-link small popup-img"
                                                        title="Fifth image title">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <div class="overlay-container">
                                                    <img src="../../upload/products/product01.jpg" alt="">
                                                    <a href="../../upload/products/product01.jpg" class="overlay-link small popup-img"
                                                        title="Fifth image title">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <div class="overlay-container">
                                                    <img src="../../upload/products/product_pic.jpg" alt="">
                                                    <a href="../../upload/products/product_pic.jpg" class="overlay-link small popup-img"
                                                        title="Fifth image title">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 bg-gray p-3 p-lg-5">
                                    <div class="product-detail-content">
                                        <div class="prod-description">
                                            <h4 class="mb-4">產品描述</h4>
                                            <div class="editor-wrap">
                                                <p>適用卷材裁切設備，依照尺寸需求，可單刀片進行定尺寸過分流機構，將片材依續堆疊；</p>
                                                <p>如特殊功能需要將單刀片進行定尺寸切，也可搭配專abc屬治具單刀片進行定尺寸。 </p>
                                                <ul>
                                                    <li>單刀材裁切寸</li>
                                                    <li>適用片單刀片進行定尺寸，使用單刀片進行定尺寸半切或全切，也能單刀片進行定尺寸分別不同尺寸切割。</li>
                                                    <li>)適用單刀片進行定尺寸，使用單刀片進行定尺寸半切或全切單刀片進行定尺寸切割。</li>
                                                </ul>
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">#</th>
                                                                <th scope="col">Heading</th>
                                                                <th scope="col">Heading</th>
                                                                <th scope="col">Heading</th>
                                                                <th scope="col">Heading</th>
                                                                <th scope="col">Heading</th>
                                                                <th scope="col">Heading</th>
                                                                <th scope="col">Heading</th>
                                                                <th scope="col">Heading</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">1</th>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">2</th>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">3</th>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                                <td>Cell</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 text-center py-3">
                                    <a href="list.php" class="btn btn-default-h radius-50">回列表</a>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>

        <?php include "../includes/footer.php" ?>
    </div>
    <!-- page-wrapper end -->

    <!--Plugins-->
    <!-- Jquery and Bootstap core js files -->
    <script src="../../assets/plugins/jquery.min.js"></script>
    <script src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Appear javascript -->
    <script src="../../assets/plugins/waypoints/jquery.waypoints.min.js"></script>
    <script src="../../assets/plugins/waypoints/sticky.min.js"></script>
    <!-- Magnific Popup javascript -->
    <script src="../../assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Slick carousel javascript -->
    <script src="../../assets/plugins/slick/slick.min.js"></script>
    <!-- Initialization of Plugins -->
    <script src="../../assets/js/template.js"></script>
    <!-- Custom Scripts -->
    <script src="../../assets/js/clicker_box.js"></script>
    <script src="../../assets/js/custom.js"></script>

</body>

</html>