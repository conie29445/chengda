<!DOCTYPE html>
<html lang="zh-tw">
<?php include "../includes/in-head.php" ?>

<body>

    <!-- scrollToTop -->
    <!-- ================ -->
    <div class="scrollToTop circle"><i class="fa fa-angle-up"></i></div>

    <!-- page wrapper start -->
    <!-- ================ -->
    <div class="page-wrapper">

        <?php include "../includes/header.php" ?>

        <div class="banner in-banner dark-translucent-bg" style="background-image: url(../../assets/images/home/bg-idx-about.jpg);">
            <div class="container">
                <div class="row pv-4r">
                    <div class="col-lg-7">
                        <div class="banner-context text-left">
                            <h2 class="page-title title text-default mb-3" data-animation-effect="fadeIn"
                                data-effect-delay="100">產品介紹</h2>
                            <p data-animation-effect="fadeIn" data-effect-delay="100">分類1</p>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="dark">
                            <ol class="breadcrumb d-flex justify-content-lg-end">
                                <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="index.html">首頁</a></li>
                                <li class="breadcrumb-item">產品介紹</li>
                                <li class="breadcrumb-item active">分類1</li>
                            </ol>
                        </div>
                        <!-- breadcrumb end -->
                    </div>
                </div>
            </div>

        </div>
        <!-- banner end -->

        <div id="page-start"></div>

        <section class="in-wrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="aside-wrap col-lg-3 col-xl-2 page-border bg-dark dark light-gray-bg px-0">
                        <aside class="px-3">
                            <div class="sidebar py-3 py-lg-5 px-2 mt-0 mt-lg-40">
                                <h3 class="title pb-2">產品總覽</h3>
                                <div class="separator-2"></div>
                                <nav class="side-menu product-menu">
                                    <ul class="nav flex-column">
                                        <li class="nav-item"><a class="nav-link active" href="list.php">分類1</a></li>
                                        <li class="nav-item"><a class="nav-link" href="list.php">分類2</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </aside>
                    </div>
                    <div class="col-lg-9 col-xl-10 p-0">
                        <div class="product-search-wrap bg-gray">
                            <div class="px-3 py-1 d-flex justify-content-between justify-content-lg-end align-items-center">
                                <div id="btn-aside-trigger" class="d-flex justify-content-center align-items-center d-lg-none px-2 px-sm-3">產品選單<span
                                        class="fa fa-bars pl-1"></span></div>

                                <?php include "../pages/product-search.php" ?>
                            </div>
                        </div>
                        <section id="projects-wrap">
                            <div class="container-fluid">
                                <div class="row grid-space-0 products-list">
                                    <div class="col-12 col-md-6 col-lg-4 col-xl-3 product-item">
                                        <div class="product-content">
                                            <div class="product-image">
                                                <img src="../../upload/products/product01.jpg" alt="正大尾門油壓升降機有限公司">
                                            </div>
                                            <div class="detail-link product-detail-link d-flex flex-column justify-content-center align-items-center">
                                                <a title="正大尾門" href="detail.php" class="btn btn-default radius-50 letter-space-02">
                                                    <span>詳情</span>
                                                </a>
                                            </div>
                                            <div class="py-3 px-4">
                                                <a href="detail.php"><span>產品名稱</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-4 col-xl-3 product-item">
                                        <div class="product-content">
                                            <div class="product-image">
                                                <img src="../../upload/products/product02.jpg" alt="正大尾門油壓升降機有限公司">
                                            </div>
                                            <div class="detail-link product-detail-link d-flex flex-column justify-content-center align-items-center">
                                                <a title="正大尾門" href="detail.php" class="btn btn-default radius-50 letter-space-02">
                                                    <span>詳情</span>
                                                </a>
                                            </div>
                                            <div class="py-3 px-4">
                                                <a href="detail.php"><span>產品名稱產品名稱</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-4 col-xl-3 product-item">
                                        <div class="product-content">
                                            <div class="product-image">
                                                <img src="../../upload/products/product03.jpg" alt="正大尾門油壓升降機有限公司">
                                            </div>
                                            <div class="detail-link product-detail-link d-flex flex-column justify-content-center align-items-center">
                                                <a title="正大尾門" href="detail.php" class="btn btn-default radius-50 letter-space-02">
                                                    <span>詳情</span>
                                                </a>
                                            </div>
                                            <div class="py-3 px-4">
                                                <a href="detail.php"><span>產品名稱產品名稱產品名稱產品名稱產品名稱產品名稱產品名稱產品名稱</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-4 col-xl-3 product-item">
                                        <div class="product-content">
                                            <div class="product-image">
                                                <img src="../../upload/products/product04.jpg" alt="正大尾門油壓升降機有限公司">
                                            </div>
                                            <div class="detail-link product-detail-link d-flex flex-column justify-content-center align-items-center">
                                                <a title="正大尾門" href="detail.php" class="btn btn-default radius-50 letter-space-02">
                                                    <span>詳情</span>
                                                </a>
                                            </div>
                                            <div class="py-3 px-4">
                                                <a href="detail.php"><span>產品名稱</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-4 col-xl-3 product-item">
                                        <div class="product-content">
                                            <div class="product-image">
                                                <img src="../../upload/products/product05.jpg" alt="正大尾門油壓升降機有限公司">
                                            </div>
                                            <div class="detail-link product-detail-link d-flex flex-column justify-content-center align-items-center">
                                                <a title="正大尾門" href="detail.php" class="btn btn-default radius-50 letter-space-02">
                                                    <span>詳情</span>
                                                </a>
                                            </div>
                                            <div class="py-3 px-4">
                                                <a href="detail.php"><span>產品名稱</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-4 col-xl-3 product-item">
                                        <div class="product-content">
                                            <div class="product-image">
                                                <img src="../../upload/products/product01.jpg" alt="正大尾門油壓升降機有限公司">
                                            </div>
                                            <div class="detail-link product-detail-link d-flex flex-column justify-content-center align-items-center">
                                                <a title="正大尾門" href="detail.php" class="btn btn-default radius-50 letter-space-02">
                                                    <span>詳情</span>
                                                </a>
                                            </div>
                                            <div class="py-3 px-4">
                                                <a href="detail.php"><span>產品名稱</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-4 col-xl-3 product-item">
                                        <div class="product-content">
                                            <div class="product-image">
                                                <img src="../../upload/products/product02.jpg" alt="正大尾門油壓升降機有限公司">
                                            </div>
                                            <div class="detail-link product-detail-link d-flex flex-column justify-content-center align-items-center">
                                                <a title="正大尾門" href="detail.php" class="btn btn-default radius-50 letter-space-02">
                                                    <span>詳情</span>
                                                </a>
                                            </div>
                                            <div class="py-3 px-4">
                                                <a href="detail.php"><span>產品名稱</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-4 col-xl-3 product-item">
                                        <div class="product-content">
                                            <div class="product-image">
                                                <img src="../../upload/products/product03.jpg" alt="正大尾門油壓升降機有限公司">
                                            </div>
                                            <div class="detail-link product-detail-link d-flex flex-column justify-content-center align-items-center">
                                                <a title="正大尾門" href="detail.php" class="btn btn-default radius-50 letter-space-02">
                                                    <span>詳情</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <nav aria-label="Page navigation">
                                    <ul class="pagination justify-content-center">
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>
                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>

        <?php include "../includes/footer.php" ?>
    </div>
    <!-- page-wrapper end -->

    <!--Plugins-->
    <!-- Jquery and Bootstap core js files -->
    <script src="../../assets/plugins/jquery.min.js"></script>
    <script src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Appear javascript -->
    <script src="../../assets/plugins/waypoints/jquery.waypoints.min.js"></script>
    <script src="../../assets/plugins/waypoints/sticky.min.js"></script>
    <!-- Slick carousel javascript -->
    <script src="../../assets/plugins/slick/slick.min.js"></script>
    <!-- Initialization of Plugins -->
    <script src="../../assets/js/template.js"></script>
    <!-- Custom Scripts -->
    <script src="../../assets/js/clicker_box.js"></script>
    <script src="../../assets/js/custom.js"></script>

</body>

</html>