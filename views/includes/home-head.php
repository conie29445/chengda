<head>
    <meta charset="utf-8">
    <meta name="author" content="4pear" />
    <meta name="description" content="正大尾門|專門製造各式、特殊之尾門升降機、單缸、雙缸、五缸等，材質包含黑鐵、白鐵以及鋁合金尾門製造，服務項目：國外各品牌尾門銷售、安裝、維修、尾門配件銷售、鋁合金車體製造等">
    <!-- Document title -->
    <title>升降機、尾門、尾門油壓升降機、正大尾門油壓升降機有限公司</title>

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="shortcut icon" href="../../favicon.ico">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">

    <!-- Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Serif" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="../../assets/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Font Awesome CSS -->
    <link href="../../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Plugins -->
    <link href="../../assets/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="../../assets/plugins/rs-plugin-5/css/settings.css" rel="stylesheet">
    <link href="../../assets/plugins/rs-plugin-5/css/layers.css" rel="stylesheet">
    <link href="../../assets/plugins/rs-plugin-5/css/navigation.css" rel="stylesheet">
    <link href="../../assets/css/animations.css" rel="stylesheet">
    <link href="../../assets/plugins/slick/slick.css" rel="stylesheet">
    
    <link href="../../assets/css/style.css" rel="stylesheet" >
    <link href="../../assets/css/typography-default.css" rel="stylesheet" >

    <!-- Custom css -->
    <link href="../../assets/css/custom/custom-home.css" rel="stylesheet" type="text/css" media="screen">

</head>