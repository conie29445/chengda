<!-- Footer -->
<footer id="footer" class="footer-light">
    <div class="footer-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 mb-4 mb-lg-0">
                    <p class="pic footer-logo-pic"><img src="../../assets/images/logo/logo-s.svg" alt=""></p>
                </div>
                <div class="offset-lg-4 col-lg-3 mb-4 mb-lg-0">
                    <!-- Footer widget area 1 -->
                    <div class="widget clearfix widget-contact-us" style="background-image: url('../../src/assets/images/world-map-dark.png'); background-position: 50% 20px; background-repeat: no-repeat">
                        <h4>正大尾門油壓升降機有限公司</h4>
                        <ul class="list-icon">
                            <li><i class="fa fa-map-marker mr-2 text-center"></i> 彰化縣伸港鄉什股路1-51號</li>
                            <li><i class="fa fa-phone mr-2 text-center"></i> 04-7988856</li>
                            <li><i class="fa fa-print mr-2 text-center"></i> 04-7986409</li>
                            <li><i class="fa fa-envelope-open mr-2 text-center"></i> <a href="mailto:chengda.taillift@gmail.com">
                                    chengda.taillift@gmail.com</a>
                            </li>
                        </ul>
                        <!-- Social icons -->
                        <div class="social-icons social-icons-border m-t-10">
                            <ul>
                                <li class="social-facebook"><a href="https://www.facebook.com/chengda.taillift/" target="_blank" class="radius-50"><i class="fa fa-facebook pr-2"></i> 追蹤</a></li>
                            </ul>
                        </div>
                        <!-- end: Social icons -->
                    </div>
                    <!-- end: Footer widget area 1 -->
                </div>
                <div class="col-lg-2">
                    <!-- Footer widget area 2 -->
                    <div class="widget">
                        <h4>主選單</h4>
                        <ul class="list-icon list-icon-arrow">
                            <li><a href="/views/about/about.php">關於正大</a></li>
                            <li><a href="/views/contact/contact.php">聯絡我們</a></li>
                            <li><a href="/views/albums/albums.php">工程實績</a></li>
                            <li><a href="/views/news/news.php">最新消息</a></li>
                        </ul>
                    </div>
                    <!-- end: Footer widget area 2 -->
                </div>

            </div>
        </div>
    </div>

    <div class="subfooter bg-dark">
        <div class="container">
            <div class="subfooter-inner">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center d-block d-md-flex justify-content-center">
                            <span>&copy; 2019 正大尾門油壓升降機有限公司. All Rights Reserved.</span>
                            <span>Designed by <a href="http://www.inspiro-media.com" target="_blank">4點梨子</a></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- end: Footer -->