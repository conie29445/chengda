<!-- header-container start -->
<div class="header-container">
    <header class="header dark fixed fixed-all clearfix">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-lg-4 col-xl-3 hidden-md-down">
                    <!-- header-first start -->
                    <!-- ================ -->
                    <div class="header-first clearfix">
                        <!-- logo -->
                        <div id="logo" class="logo">
                            <a href="../home/index.php">
                                <img src="../../assets/images/logo/logo-w.svg" alt="正大尾門油壓升降機有限公司"
                                    class="logo-img">
                            </a>
                        </div>
                    </div>
                    <!-- header-first end -->

                </div>
                <div class="col-lg-8 col-xl-9">

                    <!-- header-second start -->
                    <!-- ================ -->
                    <div class="header-second clearfix">

                        <div class="main-navigation  animated">
                            <nav class="navbar navbar-expand-lg navbar-light p-0 justify-content-center">
                                <div class="navbar-brand clearfix hidden-lg-up">
                                    <div id="logo-mobile" class="logo">
                                        <a href="../home/index.php">
                                            <img src="../../assets/images/logo/logo-w.svg" alt="正大尾門油壓升降機有限公司"
                                                class="logo-img">
                                        </a>
                                    </div>
                                </div>
                                <div id="main-menu-trigger" class="navbar-toggler navbar-toggler-right" data-toggle="collapse" data-target="#main-menu-wrap" aria-controls="main-menu-wrap" aria-expanded="false" aria-label="Toggle navigation">
                                    <div class="lines-button x"> <span class="lines"></span> </div>
                                </div>
                                <div class="collapse navbar-collapse scrollspy" id="main-menu-wrap">
                                    <!-- main-menu -->
                                    <ul class="navbar-nav ml-lg-auto">
                                        <li class="nav-item"><a class="nav-link" href="../about/about.php">關於正大</a></li>
                                        <li class="nav-item dropdown ">
                                            <a href="../product/list.php" class="nav-link dropdown-toggle" id="sixth-dropdown"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">產品介紹</a>
                                            <ul class="dropdown-menu" aria-labelledby="sixth-dropdown">
                                                <li><a href="../product/list.php">分類1</a></li>
                                                <li><a href="../product/list.php">分類2</a></li>
                                            </ul>
                                        </li>
                                        <li class="nav-item"><a class="nav-link" href="../contact/contact.php">聯絡我們</a></li>
                                        <li class="nav-item"><a class="nav-link" href="../albums/albums.php">工程實績</a></li>
                                        <li class="nav-item"><a class="nav-link" href="../news/list.php">最新消息</a></li>
                                    </ul>
                                    <!-- main-menu end -->
                                </div>
                            </nav>
                        </div>
                        <!-- main-navigation end -->

                    </div>
                    <!-- header-second end -->

                </div>
            </div>
        </div>
    </header>
    <!-- header end -->
</div>
<!-- header-container end -->